<?php

namespace Ranbogmord\TestEmail\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject(sprintf("Test email from %s", config('app.name')))
            ->markdown('test-email::emails.test', [
                'app' => config('app.name'),
                'url' => config('app.url')
            ]);
    }
}
