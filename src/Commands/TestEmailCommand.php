<?php

namespace Ranbogmord\TestEmail\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Ranbogmord\TestEmail\Mail\TestEmail;

class TestEmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:test {to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a test email to the given address to test delivery';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::to($this->argument("to"))->send(new TestEmail);
    }
}
