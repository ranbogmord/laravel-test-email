@component('mail::message')
# Test Email

This is a test email sent from {{ $app }} ({{ $url }}).
@endcomponent
