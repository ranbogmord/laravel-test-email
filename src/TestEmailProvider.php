<?php

namespace Ranbogmord\TestEmail;

use Illuminate\Support\ServiceProvider;
use Ranbogmord\TestEmail\Commands\TestEmailCommand;

class TestEmailProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadViewsFrom(__DIR__ . '/views', 'test-email');

            $this->commands([
                TestEmailCommand::class
            ]);
        }
    }
}
