# Laravel Test Email
Simple utility package to test email delivery in Laravel using an artisan command

## Installation
```
composer require ranbogmord/laravel-test-email
```

## Usage
```
php artisan email:test <receiver>
php artisan email:test test@example.com
```
